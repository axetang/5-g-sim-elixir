#!/usr/bin/python

tourDescription = """
### MERIF 2023: Teaching 5G with POWDER and OpenAirInterface5G

This profiled is based on the ''merif-5g-sim'' profile. The description and instructions
for this activity can be found
[here](https://gitlab.flux.utah.edu/powderrenewpublic/merif2023/-/blob/main/content/teaching-5g-oai.md).
"""

tourInstructions = """

Note: After you instantiate an experiment, you have to wait until the POWDER
portal shows your experiment status in green as "Your experiment is ready"
before proceeding.


"""


import geni.portal as portal
import geni.rspec.pg as rspec
import geni.urn as URN
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.pg as pg
import geni.rspec.emulab as emulab
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)

HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"

request = portal.context.makeRequestRSpec()
request.addRole(
    Role("setup",path="ansible",playbooks=[
        Playbook("setup",path="setup.yml",become=None)]))

node = request.RawPC( "node" )
node.hardware_type = "d430"
node.disk_image = "urn:publicid:IDN+emulab.net+image+mww2023:oai-cn5g-rfsim"
node.startVNC()
node.addService(pg.Execute(shell="sh",command=HEAD_CMD))
node.bindRole(RoleBinding("setup"))

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

portal.context.printRequestRSpec()
